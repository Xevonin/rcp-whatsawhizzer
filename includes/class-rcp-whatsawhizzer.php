<?php
/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * frontend-facing side of the site and the admin area.
 *
 * @link       https://xevon.in/
 * @since      1.0.0
 *
 * @package    RCP_Whatsawhizzer
 * @subpackage RCP_Whatsawhizzer/includes
 */

namespace RCP_Whatsawhizzer\includes;

use RCP_Whatsawhizzer\admin\Admin;
use RCP_Whatsawhizzer\frontend\Frontend;

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * frontend-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    RCP_Whatsawhizzer
 * @subpackage RCP_Whatsawhizzer/includes
 * @author     Your Name <email@example.com>
 */
class RCP_Whatsawhizzer {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the frontend-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {
		if ( defined( 'RCP_WHATSAWHIZZER_VERSION' ) ) {
			$this->version = RCP_WHATSAWHIZZER_VERSION;
		} else {
			$this->version = '1.0.0';
		}
		$this->plugin_name = 'rcp-time-drip';

		$this->loader = new Loader();

		$this->define_admin_hooks();
		$this->define_frontend_hooks();
	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	protected function define_admin_hooks() {
		$plugin_admin = new Admin( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'enqueue_block_editor_assets', $plugin_admin, 'enqueue_block_editor_assets' );
		$this->loader->add_action( 'rest_api_init', $plugin_admin, 'rest_api_register_meta' );
		$this->loader->add_action( 'rest_api_init', $plugin_admin, 'rest_api_register_endpoints', 11 );
		$this->loader->add_filter( 'rcp_metabox_excluded_post_types', $plugin_admin, 'remove_rcp_metabox' );
		$this->loader->add_action( 'rest_after_insert_post', $plugin_admin, 'rest_after_insert', 10, 3 );
		$this->loader->add_action( 'rest_after_insert_page', $plugin_admin, 'rest_after_insert', 10, 3 );
		$this->loader->add_action( 'added_post_meta', $plugin_admin, 'after_restriction_removal', 10, 4 );
		$this->loader->add_action( 'updated_post_meta', $plugin_admin, 'after_restriction_removal', 10, 4 );
		$this->loader->add_filter( 'rcp_messages_settings', $plugin_admin, 'rcp_messages_settings' );
	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	protected function define_frontend_hooks() {
		$plugin_frontend = new Frontend( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_filter( 'rcp_restricted_message', $plugin_frontend, 'rcp_restricted_message' );
	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

}
