<?php
/**
 * Restrict Content Pro Whatsawhizzer file
 *
 * @link    https://xevon.in/
 * @since   1.0.0
 * @package RCP_Whatsawhizzer
 *
 * @wordpress-plugin
 * Plugin Name:       Restrict Content Pro - Whatsawhizzer
 * Plugin URI:        https://xevon.in/
 * Description:       Extends Restrict Content Pro. Gutenberg support, Patreon support and time drip.
 * Version:           1.0.0
 * Author:            Xevonin
 * Author URI:        https://xevon.in/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       rcp-whatsawhizzer
 * Domain Path:       /languages
 */

namespace RCP_Whatsawhizzer;

use RCP_Whatsawhizzer\includes\RCP_Whatsawhizzer;

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

// Define the plugin file location.
if ( ! defined( 'RCP_WHATSAWHIZZER_PLUGIN_FILE' ) ) {
	define( 'RCP_WHATSAWHIZZER_PLUGIN_FILE', __FILE__ );
}

// Composer autoload.
require_once plugin_dir_path( RCP_WHATSAWHIZZER_PLUGIN_FILE ) . 'vendor/autoload.php';

// Loads all required classes.
require_once plugin_dir_path( RCP_WHATSAWHIZZER_PLUGIN_FILE ) . 'autoload.php';

// Current plugin version.
define( 'RCP_WHATSAWHIZZER_VERSION', '1.0.0' );

/**
 * Begins execution of the plugin.
 *
 * @since 1.0.0
 */
function instantiate_rcp_whatsawhizzer() {
	$plugin = new RCP_Whatsawhizzer();
	return $plugin;
}

$rcp_whatsawhizzer            = instantiate_rcp_whatsawhizzer();
$GLOBALS['rcp_whatsawhizzer'] = $rcp_whatsawhizzer;
$rcp_whatsawhizzer->run();
