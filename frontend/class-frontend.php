<?php
/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://xevon.in/
 * @since      1.0.0
 *
 * @package    RCP_Whatsawhizzer
 * @subpackage RCP_Whatsawhizzer/frontend
 */

namespace RCP_Whatsawhizzer\frontend;

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the frontend-facing stylesheet and JavaScript.
 *
 * @package    RCP_Whatsawhizzer
 * @subpackage RCP_Whatsawhizzer/frontend
 * @author     Xevonin <xevo@live.nl>
 */
class Frontend {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string $plugin_name       The name of the plugin.
	 * @param      string $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {
		$this->plugin_name = $plugin_name;
		$this->version     = $version;
	}

	/**
	 * Shows different messages based on different locking methods.
	 *
	 * @access public
	 * @since  1.0.0
	 */
	public function rcp_restricted_message( $message ) {
		global $post, $rcp_options;

		$level = rcp_get_content_subscription_levels( $post->ID );
		if ( is_array( $level ) ) {
			$message = $rcp_options['level_restricted_message'];
			$message = str_replace( '%tier%', rcp_get_membership_level_by( 'id', $level[0] )->name, $message );
		} elseif ( 'any' === $level ) {
			$message = $rcp_options['loggedin_restricted_message'];
		} elseif ( 'any-paid' === $level ) {
			$message = $rcp_options['paid_restricted_message'];
		}

		return $message;
	}

}
