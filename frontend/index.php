<?php
/**
 * Silence is golden.
 *
 * @link    https://xevon.in/
 * @since   1.0.0
 * @package RCP_Whatsawhizzer
 */

die();
