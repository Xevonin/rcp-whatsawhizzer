/**
 * External dependencies.
 */
import React from 'react'

/**
 * WordPress dependencies.
 */
const { __ } = wp.i18n;
const { compose } = wp.compose;
const { withDispatch, withSelect } = wp.data;
const { Spinner, SelectControl } = wp.components;
const { Fragment, Component } = wp.element;

class RestrictByAccessLevel extends Component {
    constructor( props ) {
        super( props )

        this.state = {
            loading: true,
            levels: []
        }
    }

    componentDidMount() {
		this.getRCPAccessLevels();
	}

    getRCPAccessLevels() {
        wp.apiFetch({
			path: 'rcpgutenberg/v1/access-levels',
		}).then(data => {
			this.setState({
				levels: data,
                loading: false
			});
		});
    }

    render() {
        return (
            <Fragment>
                { this.state.loading ? (
                        <Spinner />
                    ) : (
                        <SelectControl
                            value={ this.props.metaFieldValue ? this.props.metaFieldValue : 0 }
                            onChange={ this.props.setMetaFieldValue }
                            options={ this.state.levels }
                        />
                    )
                }
            </Fragment>
        )
    }
}

export default compose([
    withDispatch(( dispatch , props ) => {
        return {
            setMetaFieldValue: function( value ) {
                dispatch( 'core/editor' ).editPost( { meta: { rcp_access_level: value } } );
            }
        }
    }),
    withSelect(( select , props ) => {
        return {
            metaFieldValue: select( 'core/editor' ).getEditedPostAttribute( 'meta' )[ 'rcp_access_level' ],
        };
    }),
])( RestrictByAccessLevel );