/**
 * External dependencies.
 */
import React from 'react'

/**
 * WordPress dependencies.
 */
const { __ } = wp.i18n;
const { compose } = wp.compose;
const { withDispatch, withSelect } = wp.data;
const { Spinner, CheckboxControl, PanelRow } = wp.components;
const { Fragment, Component } = wp.element;

class RestrictByRole extends Component {
    constructor( props ) {
        super( props )

        this.state = {
            loading: true,
            roles: [],
            checked: this.props.metaFieldValue,
        }
    }

    componentDidMount() {
		this.getRCPRoles();
	}

    getRCPRoles() {
        wp.apiFetch({
			path: 'rcpgutenberg/v1/roles',
		}).then(data => {
			this.setState({
				roles: data,
                loading: false
			});
		});
    }

    isChecked( slug ) {
        if ( this.state.checked.includes( slug ) ) {
            return true;
        }

        return false;
    }

    setChecked( slug, check ) {
        let newChecked = this.state.checked;

        if ( check ) {
            newChecked.push( slug );
        } else {
            newChecked.splice( newChecked.indexOf( slug ), 1 ); 
        }

        this.setState({
            checked: newChecked
        });

        console.log( this.state.checked );

        this.props.setMetaFieldValue(this.state.checked);
    }

    render() {
        return (
            <Fragment>
                { this.state.loading ? (
                        <PanelRow>
                            <Spinner />
                        </PanelRow>
                    ) : (
                        <For each="role" of={ this.state.roles }>
                            {
                                React.createElement( PanelRow, null,
                                    React.createElement(
                                        CheckboxControl,
                                        {
                                            label: role.label,
                                            checked: this.isChecked( role.value ),
                                            onChange: (check) => { this.setChecked( role.value, check ) }
                                        }
                                    )
                                ) 
                            }
                        </For>
                    )
                }
            </Fragment>
        )
    }
}

export default compose([
    withDispatch(( dispatch , props ) => {
        return {
            setMetaFieldValue: function( value ) {
                dispatch( 'core/editor' ).editPost( { meta: { rcp_user_level: value } } );
            }
        }
    }),
    withSelect(( select , props ) => {
        return {
            metaFieldValue: select( 'core/editor' ).getEditedPostAttribute( 'meta' )[ 'rcp_user_level' ],
        };
    }),
])( RestrictByRole );