/**
 * External dependencies.
 */
import React from 'react'

/**
 * WordPress dependencies.
 */
const { __ } = wp.i18n;
const { compose } = wp.compose;
const { withDispatch, withSelect } = wp.data;
const { DateTimePicker, Button, Dropdown, PanelRow } = wp.components;
const { Fragment, Component } = wp.element;
const { date } = wp.date;

/**
 * Browser title input component.
 *
 * @since 1.0.0
 */
class RestrictRemoval extends Component {  
    constructor( props ) {
        super( props )
    }

    render() {
        return (
            <Fragment>
                <PanelRow>
                    <p>Select the date at which you wish to remove all restrictions automatically.</p>
                </PanelRow>
                <PanelRow>
                    <Dropdown
                        position="middle left"
                        renderToggle={ ( { isOpen, onToggle } ) => (
                            <Button isPrimary onClick={ onToggle } aria-expanded={ isOpen }>
                                { this.props.metaFieldValue ? date( 'd-m-Y H:i', this.props.metaFieldValue ) : 'Select Date' }
                            </Button>
                        ) }
                        renderContent={ () => (
                            <DateTimePicker
                                currentDate={ this.props.metaFieldValue ? this.props.metaFieldValue : new Date() }
                                onChange={ ( date ) => this.props.setMetaFieldValue( date ) }
                            />
                        ) }
                    />
                </PanelRow>
            </Fragment>
        )
    }
}

export default compose([
    withDispatch(( dispatch , props ) => {
        return {
            setMetaFieldValue: function( value ) {
                // value = date( 'Y-m-d H:i:s', value );
                dispatch( 'core/editor' ).editPost( { meta: { rcp_timeout: value } } );
            }
        }
    }),
    withSelect(( select , props ) => {
        return {
            metaFieldValue: select( 'core/editor' ).getEditedPostAttribute( 'meta' )[ 'rcp_timeout' ],
        };
    }),
])( RestrictRemoval );