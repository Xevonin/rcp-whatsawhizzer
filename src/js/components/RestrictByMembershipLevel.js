/**
 * External dependencies.
 */
import React from 'react'

/**
 * WordPress dependencies.
 */
const { __ } = wp.i18n;
const { compose } = wp.compose;
const { withDispatch, withSelect } = wp.data;
const { Spinner, CheckboxControl, PanelRow } = wp.components;
const { Component } = wp.element;

/**
 * Browser title input component.
 *
 * @since 1.0.0
 */
class RestrictByMembershipLevel extends Component {  
    constructor( props ) {
        super( props )

        this.state = {
            loading: true,
            levels: [],
            checked: []
        }
    }

    componentDidMount() {
        if( this.props.metaFieldValue != null ) {
            this.state.checked = this.props.metaFieldValue;
        }
        
		this.getRCPLevels();
	}

    getRCPLevels() {
        wp.apiFetch({
			path: 'rcpgutenberg/v1/levels',
		}).then(data => {
			this.setState({
				levels: data,
                loading: false
			});
		});
    }

    isChecked( id ) {
        if ( this.state.checked.includes( id ) ) {
            return true;
        }

        return false;
    }

    setChecked( id, check ) {
        let newChecked = this.state.checked;

        if ( check ) {
            newChecked.push( id );
        } else {
            newChecked.splice( newChecked.indexOf( id ), 1 ); 
        }

        this.setState({
            checked: newChecked
        });

        this.props.setMetaFieldValue( this.state.checked );
    }

    render() {
        return (
            <div className="rcp-restrict-by-membershiplevel">
                { this.state.loading ? (
                        <Spinner />
                    ) : (
                        <For each="level" of={ this.state.levels }>
                            {
                                React.createElement( PanelRow, null,
                                    React.createElement(
                                        CheckboxControl,
                                        {
                                            label: level.name,
                                            checked: this.isChecked( level.id ),
                                            onChange: (check) => { this.setChecked( level.id, check ) }
                                        }
                                    )
                                )
                            }
                        </For>
                    )
                }
            </div>
        )
    }
}

export default compose([
    withDispatch(( dispatch , props ) => {
        return {
            setMetaFieldValue: function( value ) {
                dispatch( 'core/editor' ).editPost( { meta: { rcp_subscription_level: value } } );
            }
        }
    }),
    withSelect(( select , props ) => {
        return {
            metaFieldValue: select( 'core/editor' ).getEditedPostAttribute( 'meta' )[ 'rcp_subscription_level' ],
        };
    }),
])( RestrictByMembershipLevel );