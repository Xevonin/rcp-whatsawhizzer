/**
 * External dependencies.
 */
import React from 'react'

/**
 * WordPress dependencies.
 */
const { __ } = wp.i18n;
const { compose } = wp.compose;
const { withDispatch, withSelect } = wp.data;
const { Fragment, Component } = wp.element;
const { PluginSidebarMoreMenuItem, PluginSidebar } = wp.editPost;
const { Panel, PanelBody, PanelRow, SelectControl } = wp.components;

/**
 * Local dependencies.
 */
import RestrictByMembershipLevel from './RestrictByMembershipLevel.js';
import RestrictByAccessLevel from './RestrictByAccessLevel.js';
import RestrictByRole from './RestrictByRole.js';
import RestrictByTime from './RestrictByTime.js';
import RestrictRemoval from './RestrictRemoval.js';

/**
 * Sidebar component voor the gutenberg editor.
 */
class Sidebar extends Component {
    constructor( props ) {
        super( props )
    }

    render() {
        return (
            <Fragment>
                <PluginSidebarMoreMenuItem target="rcp-sidebar" icon=''>
                    {__("Restrict this content", "rcptimedrip")}
                </PluginSidebarMoreMenuItem>
                <PluginSidebar name="rcp-sidebar" title={__("Restrict this content", "rcptimedrip")} >
                    <Panel>
                        <PanelBody title="Member access options" initialOpen={ true }>
                            <PanelRow>
                                <SelectControl
                                    value={ this.props.metaFieldValue ? this.props.metaFieldValue : 'unrestricted' }
                                    onChange={ this.props.setMetaFieldValue }
                                    options={ [
                                        { value: 'unrestricted', label: 'No restrictions' },
                                        { value: 'subscription-level', label: 'Restricy by Member Level' },
                                        { value: 'access-level', label: 'Restrict by Access Level' },
                                        { value: 'registered-users', label: 'Restrict by Role' },
                                    ] }
                                />
                            </PanelRow>
                        </PanelBody>
                    </Panel>

                    <If condition={ this.props.metaFieldValue == 'subscription-level' }>
                        <Panel>
                            <PanelBody title="Member Level options" initialOpen={ true }>
                                <PanelRow>
                                    <RestrictByMembershipLevel/>
                                </PanelRow>
                            </PanelBody>
                        </Panel>

                        <Panel>
                            <PanelBody title="Time access options" initialOpen={ false }>
                                <RestrictByTime/>
                            </PanelBody>
                        </Panel>
                    </If>

                    <If condition={ this.props.metaFieldValue == 'access-level' }>
                        <Panel>
                            <PanelBody title="Access Level options" initialOpen={ true }>
                                <PanelRow>
                                    <RestrictByAccessLevel/>
                                </PanelRow>
                            </PanelBody>
                        </Panel>
                    </If>
                    
                    <If condition={ this.props.metaFieldValue == 'registered-users' }>
                        <Panel>
                            <PanelBody title="Role access options" initialOpen={ true }>
                                <RestrictByRole/>
                            </PanelBody>
                        </Panel>
                    </If>
                    
                    <If condition={ this.props.metaFieldValue != 'unrestricted' }>
                        <Panel>
                            <PanelBody title="Restriction removal options" initialOpen={ false }>
                                <RestrictRemoval/>
                            </PanelBody>
                        </Panel>
                    </If>
                </PluginSidebar>
            </Fragment>
        )
    }
}

export default compose([
    withDispatch(( dispatch , props ) => {
        return {
            setMetaFieldValue: function( value ) {
                dispatch( 'core/editor' ).editPost( { meta: { rcp_restrict_by: value } } );
            }
        }
    }),
    withSelect(( select , props ) => {
        return {
            metaFieldValue: select( 'core/editor' ).getEditedPostAttribute( 'meta' )[ 'rcp_restrict_by' ],
        };
    }),
])( Sidebar );