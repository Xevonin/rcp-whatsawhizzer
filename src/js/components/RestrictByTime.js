/**
 * External dependencies.
 */
import React from 'react'

/**
 * WordPress dependencies.
 */
const { __ } = wp.i18n;
const { compose } = wp.compose;
const { withDispatch, withSelect } = wp.data;
const { Spinner, DateTimePicker, Button, Dropdown, PanelRow } = wp.components;
const { Component } = wp.element;
const { date } = wp.date;

/**
 * Browser title input component.
 *
 * @since 1.0.0
 */
class RestrictByTime extends Component {  
    constructor( props ) {
        super( props )

        this.state = {
            loading: true,
            levels: [],
            times: {},
            today: new Date(),
            now: ''
        }
    }

    componentDidMount() {
        if ( this.props.metaFieldValue != null && this.props.metaFieldValue.length > 0 ) {
            this.state.times = this.props.metaFieldValue;
        }

        this.getRCPLevels();
    }

    getRCPLevels() {
        wp.apiFetch({
            path: 'rcpgutenberg/v1/levels',
        }).then(data => {
            this.setState({
                levels: data,
                loading: false
            });
        });
    }

    isButtonDateSet( id ) {
        if ( this.state.times.hasOwnProperty( id ) ) {
            return date( 'Y-m-d H:i', this.state.times[id] );
        }

        return 'Select Date';
    }

    isDateSet( id ) {
        if ( this.state.times.hasOwnProperty( id ) ) {
            return this.state.times[id];
        }

        return this.state.today;
    }

    setDate( id, date ) {
        let newTimes = this.state.times;

        if ( date != null ) {
            newTimes[id] = date;
        } else {
            delete newTimes[id];
        }

        this.setState({
            times: newTimes
        });

        this.props.setMetaFieldValue( this.state.times );
    }

    render() {
        return (
            <div className="rcp-restrict-by-membershiplevel">
                { this.state.loading ? (
                        <PanelRow>
                            <Spinner />
                        </PanelRow>
                    ) : (
                        <For each="level" of={ this.state.levels }>
                            {
                                React.createElement( PanelRow, null,
                                    React.createElement( 'span', null, level.name ),
                                    React.createElement( 
                                        Dropdown, 
                                        {
                                            position: 'middle left',
                                            renderToggle: ( ( { isOpen, onToggle } ) => (
                                                React.createElement( Button, {
                                                    isLink: true,
                                                    onClick: onToggle,
                                                    ariaExpanded: isOpen
                                                }, this.isButtonDateSet( level.id ) )
                                            ) ),
                                            renderContent: () => (
                                                React.createElement( DateTimePicker, {
                                                    currentDate: this.isDateSet( level.id ),
                                                    onChange: (date) => this.setDate( level.id, date )
                                                } )
                                            )
                                        } 
                                    )
                                )
                            }
                        </For>
                    )
                }
            </div>
        )
    }
}

export default compose([
    withDispatch(( dispatch , props ) => {
        return {
            setMetaFieldValue: function( value ) {
                if( Object.keys(value).length === 0 ) {
                    value = null;
                }

                dispatch( 'core/editor' ).editPost( { meta: { rcp_time_drip: value } } );
            }
        }
    }),
    withSelect(( select , props ) => {
        return {
            metaFieldValue: select( 'core/editor' ).getEditedPostAttribute( 'meta' )[ 'rcp_time_drip' ],
        };
    }),
])( RestrictByTime );