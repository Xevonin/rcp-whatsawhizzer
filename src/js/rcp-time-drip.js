/**
 * WordPress dependencies.
 */
const { registerPlugin } = wp.plugins;
const { PluginDocumentSettingPanel } = wp.editPost
const { useSelect, registerStore } = wp.data;

/**
 * Local dependencies.
 */
import Sidebar from './components/sidebar.js';

/**
 * Register the MetaTags plugin.
 */
registerPlugin( 'rcp-time-drip', {
    icon: 'lock',
    render: Sidebar
});
