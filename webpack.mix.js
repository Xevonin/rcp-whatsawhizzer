const mix = require('laravel-mix');

const externals = {
   'lodash': 'lodash',
   'react': 'React',
   'react-dom': 'ReactDOM',
   '@wordpress/blocks': 'wp.blocks',
   '@wordpress/components': 'wp.components',
   '@wordpress/compose': 'wp.compose',
   '@wordpress/data': 'wp.data',
   '@wordpress/date': 'wp.date',
   '@wordpress/editor': 'wp.editor',
   '@wordpress/element': 'wp.element',
   '@wordpress/hooks': 'wp.hooks',
   '@wordpress/i18n': 'wp.i18n',
   '@wordpress/plugins': 'wp.plugins',
}

mix.webpackConfig({
   externals: externals,
});

mix.js('src/js/rcp-time-drip.js', 'admin/assets/js').react();
