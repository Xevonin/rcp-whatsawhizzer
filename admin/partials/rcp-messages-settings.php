<tr valign="top">
	<th>
		<label for="rcp_settings[level_restricted_message]">Level Restricted Message</label>
	</th>
	<td>
		<?php
		$level_restricted_message = isset( $rcp_options['level_restricted_message'] ) ? $rcp_options['level_restricted_message'] : '';
		wp_editor(
			$level_restricted_message,
			'rcp_level_restricted_message',
			array(
				'textarea_name' => 'rcp_settings[level_restricted_message]',
				'teeny'         => true,
			)
		);
		?>
		<p class="description">This is the message shown to users who are trying to access content with one or more level restrictions.</p>
	</td>
</tr>
<tr valign="top">
	<th>
		<label for="rcp_settings[loggedin_restricted_message]">Loggedin Restricted Message</label>
	</th>
	<td>
		<?php
		$loggedin_restricted_message = isset( $rcp_options['loggedin_restricted_message'] ) ? $rcp_options['loggedin_restricted_message'] : '';
		wp_editor(
			$loggedin_restricted_message,
			'rcp_loggedin_restricted_message',
			array(
				'textarea_name' => 'rcp_settings[loggedin_restricted_message]',
				'teeny'         => true,
			)
		);
		?>
		<p class="description">This is the message shown to logged out users who are trying to access content that requires one to be logged in.</p>
	</td>
</tr>
<tr valign="top">
	<th>
		<label for="rcp_settings[paid_restricted_message]">Paid Restricted Message</label>
	</th>
	<td>
		<?php
		$paid_restricted_message = isset( $rcp_options['paid_restricted_message'] ) ? $rcp_options['paid_restricted_message'] : '';
		wp_editor(
			$paid_restricted_message,
			'rcp_paid_restricted_message',
			array(
				'textarea_name' => 'rcp_settings[paid_restricted_message]',
				'teeny'         => true,
			)
		);
		?>
		<p class="description">This is the message shown to users who are trying to access content that requires them to have any type of paid membership.</p>
	</td>
</tr>
