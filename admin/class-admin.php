<?php
/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://xevon.in/
 * @since      1.0.0
 *
 * @package    RCP_Whatsawhizzer
 * @subpackage RCP_Whatsawhizzer/admin
 */

namespace RCP_Whatsawhizzer\admin;

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    RCP_Whatsawhizzer
 * @subpackage RCP_Whatsawhizzer/admin
 * @author     Xevonin <xevo@live.nl>
 */
class Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string $plugin_name       The name of this plugin.
	 * @param      string $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {
		$this->plugin_name = $plugin_name;
		$this->version     = $version;
	}

	/**
	 * Register custom meta's to be used in the REST API.
	 *
	 * @since 1.0.0
	 */
	public function rest_api_register_meta() {
		register_meta(
			'post',
			'rcp_restrict_by',
			array(
				'type'         => 'string',
				'single'       => true,
				'default'      => 'unrestricted',
				'show_in_rest' => true,
			)
		);

		register_meta(
			'post',
			'rcp_subscription_level',
			array(
				'type'         => 'array',
				'single'       => true,
				'default'      => array(),
				'show_in_rest' => array(
					'schema' => array(
						'type'  => 'array',
						'items' => array(
							'type' => 'integer',
						),
					),
				),
			)
		);

		register_meta(
			'post',
			'rcp_access_level',
			array(
				'type'         => 'integer',
				'single'       => true,
				'default'      => 0,
				'show_in_rest' => true,
			)
		);

		register_meta(
			'post',
			'rcp_user_level',
			array(
				'type'         => 'array',
				'single'       => true,
				'default'      => array( 'all' ),
				'show_in_rest' => array(
					'schema' => array(
						'type'  => 'array',
						'items' => array(
							'type' => 'string',
						),
					),
				),
			)
		);

		register_meta(
			'post',
			'rcp_time_drip',
			array(
				'type'         => 'object',
				'single'       => true,
				'show_in_rest' => array(
					'schema' => array(
						'type'                 => 'object',
						'additionalProperties' => true,
					),
				),
			)
		);

		register_meta(
			'post',
			'rcp_timeout',
			array(
				'type'         => 'string',
				'single'       => true,
				'default'      => '',
				'show_in_rest' => true,
			)
		);
	}

	/**
	 * Register custom endpoints to be used in the REST API.
	 *
	 * @since 1.0.0
	 */
	public function rest_api_register_endpoints() {
		register_rest_route(
			'rcpgutenberg/v1',
			'levels',
			array(
				'methods'             => 'GET',
				'callback'            => array( $this, 'rest_api_get_rcp_levels' ),
				'permission_callback' => '__return_true',
			)
		);

		register_rest_route(
			'rcpgutenberg/v1',
			'access-levels',
			array(
				'methods'             => 'GET',
				'callback'            => array( $this, 'rest_api_get_rcp_access_levels' ),
				'permission_callback' => '__return_true',
			)
		);

		register_rest_route(
			'rcpgutenberg/v1',
			'roles',
			array(
				'methods'             => 'GET',
				'callback'            => array( $this, 'rest_api_get_rcp_roles' ),
				'permission_callback' => '__return_true',
			)
		);
	}

	/**
	 * Get the Restrict Content Pro Levels to be used in the REST API.
	 *
	 * @since 1.0.0
	 */
	public function rest_api_get_rcp_levels() {
		$data   = array();
		$levels = new \RCP_Levels();
		$levels = $levels->get_levels(
			array(
				'status'  => 'active',
				'orderby' => 'price',
			)
		);

		foreach ( $levels as $level ) {
			$data[] = array(
				'id'     => $level->id,
				'name'   => $level->name,
				'price'  => $level->price,
				'status' => $level->status,
			);
		}

		usort(
			$data,
			function( $a, $b ) {
				return $a['price'] <=> $b['price'];
			}
		);

		$response = new \WP_REST_Response( $data );
		$response->set_status( 200 );

		return $response;
	}

	/**
	 * Get the Restrict Content Pro Access Levels to be used in the REST API.
	 *
	 * @since 1.0.0
	 */
	public function rest_api_get_rcp_access_levels() {
		$data   = array();
		$levels = rcp_get_access_levels();

		foreach ( $levels as $key => $level ) {
			$data[] = array(
				'value' => $key,
				'label' => $key . ' and higher',
			);
		}

		$response = new \WP_REST_Response( $data );
		$response->set_status( 200 );

		return $response;
	}

	/**
	 * Get the editable WordPress roles to be used in the REST API.
	 * TODO: Find out if this list can be retrieved from Gutenberg.
	 *
	 * @since 1.0.0
	 */
	public function rest_api_get_rcp_roles() {
		$data  = array(
			array(
				'value' => 'all',
				'label' => 'Any',
			),
		);
		$roles = wp_roles()->roles;

		$editable_roles = apply_filters( 'editable_roles', $roles );

		foreach ( $editable_roles as $slug => $role ) {
			$data[] = array(
				'value' => $slug,
				'label' => $role['name'],
			);
		}

		$response = new \WP_REST_Response( $data );
		$response->set_status( 200 );

		return $response;
	}

	/**
	 * Add our assets to the Gutenberg editor.
	 *
	 * @since 1.0.0
	 */
	public function enqueue_block_editor_assets() {
		wp_enqueue_script(
			$this->plugin_name,
			plugin_dir_url( RCP_WHATSAWHIZZER_PLUGIN_FILE ) . 'admin/assets/js/' . $this->plugin_name . '.js',
			array(
				'wp-i18n',
				'wp-blocks',
				'wp-edit-post',
				'wp-element',
				'wp-editor',
				'wp-components',
				'wp-data',
				'wp-plugins',
				'wp-edit-post',
			),
			'1.0.13',
			true
		);
	}

	/**
	 * Remove the classic RCP metabox in favor of the Gutenberg sidebar.
	 *
	 * @since 1.0.0
	 */
	public function remove_rcp_metabox( $post_types ) {
		$post_types[] = 'page';
		$post_types[] = 'post';

		return $post_types;
	}

	/**
	 * Because of a limitation in Gutenberg we are forced to remove certain metadata after save.
	 *
	 * @since 1.0.0
	 */
	public function rest_after_insert( $post, $request, $creating ) {
		$rcp_restriction = get_post_meta( $post->ID, 'rcp_restrict_by', true );

		// Check if there are any restriction on the current post.
		if ( $rcp_restriction ) {
			// Based on the restriction change or delete the metas of the post.
			switch ( $rcp_restriction ) {
				case 'unrestricted':
					delete_post_meta( $post->ID, 'rcp_subscription_level' );
					delete_post_meta( $post->ID, 'rcp_access_level' );
					delete_post_meta( $post->ID, 'rcp_user_level' );
					delete_post_meta( $post->ID, 'rcp_time_drip' );
					delete_post_meta( $post->ID, 'rcp_timeout' );
					break;
				case 'subscription-level':
					delete_post_meta( $post->ID, 'rcp_access_level' );
					delete_post_meta( $post->ID, 'rcp_user_level' );
					break;
				case 'access-level':
					delete_post_meta( $post->ID, 'rcp_subscription_level' );
					delete_post_meta( $post->ID, 'rcp_user_level' );
					delete_post_meta( $post->ID, 'rcp_time_drip' );
					break;
				case 'registered-users':
					delete_post_meta( $post->ID, 'rcp_subscription_level' );
					delete_post_meta( $post->ID, 'rcp_access_level' );
					delete_post_meta( $post->ID, 'rcp_time_drip' );
					break;
			}
		}
	}

	/**
	 * After the restriction was removed we will turn rcp_restrict_by into unrestricted.
	 *
	 * @since 1.0.0
	 */
	public function after_restriction_removal( $meta_id, $post_id, $meta_key, $meta_value ) {
		if ( 'rcp_timeout_was' === $meta_key ) {
			update_post_meta( $post_id, 'rcp_restrict_by', 'unrestricted' );
		}
	}

	/**
	 * Adds the capability to set special messages for every locking type.
	 *
	 * @since 1.0.0
	 */
	public function rcp_messages_settings( $rcp_options ) {
		include_once dirname( RCP_WHATSAWHIZZER_PLUGIN_FILE ) . '/admin/partials/rcp-messages-settings.php';
	}

}
