<?php
/**
 * Loads all required classes
 *
 * Uses classmap, PSR4 & wp-namespace-autoloader.
 *
 * @link    https://xevon.in/
 * @since   1.0.0
 * @package RCP_Whatsawhizzer
 *
 * @see https://github.com/pablo-sg-pacheco/wp-namespace-autoloader/
 */

namespace RCP_Whatsawhizzer;

use Pablo_Pacheco\WP_Namespace_Autoloader\WP_Namespace_Autoloader;

$autoloader = new WP_Namespace_Autoloader();
$autoloader->init();
